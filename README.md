Role Anaconda
=========

Ansible role to install silently Anaconda3.

Compatibility
------------

This role can be used only on Ubuntu 18 and 20. Other OS will come later

Variables Used
------------

You can find in defaults/main.yml all variables used in tasks

| Variable           | Default Value                     | Type   | Description                                |
| ------------------ | :-------------------------------- | :----- | ------------------------------------------ |
| anaconda_dest      | /tmp/anaconda                     | String | Path where anaconda installer is stored    |
| anaconda_installer | Anaconda3-2022.05-Linux-x86_64.sh | String | Anaconda installer                         |
| script_path        | /etc/profile.d/conda.sh           | String | Path where the conda init script is stored |
| installed_path     | /usr/local/anaconda3              | String | Path where anaconda is installed           |

## Author Information

Written by [Dimitri Colier](mailto:dimitri.colier@epfl.ch) for EPFL - STI school of engineering
